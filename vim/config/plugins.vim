" -----------------------------------------------------------------------------
"
" Plugins
"
" -----------------------------------------------------------------------------

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" Essentials
Plugin 'VundleVim/Vundle.vim'
Plugin 'tpope/vim-sensible'
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-git'
Plugin 'tpope/vim-fugitive'
Plugin 'ctrlpvim/ctrlp.vim'
"Plugin 'arcticicestudio/nord-vim'
"Plugin 'valloric/youcompleteme'

" Nice to have
Plugin 'mhinz/vim-signify'			" Better version of GitGutter
Plugin 'ryanoasis/vim-devicons'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'tpope/vim-markdown'
Plugin 'dhruvasagar/vim-table-mode'
Plugin 'scrooloose/nerdtree'
Plugin 'gilsondev/searchtasks.vim'
Plugin 'nathanaelkane/vim-indent-guides'
Plugin 'junegunn/limelight.vim'			" Highlight active section under cursor (requires 256color terminal in tmux
"Plugin 'jnurmine/zenburn'
Plugin 'bfontaine/brewfile.vim'
"Plugin 'nblock/vim-dokuwiki'
Plugin 'marciomazza/vim-brogrammer-theme'

" Testing
Plugin 'sickill/vim-monokai'
Plugin 'jceb/vim-orgmode'
Plugin 'godlygeek/tabular'
Plugin 'morhetz/gruvbox'
Plugin 'kien/rainbow_parentheses.vim'
Plugin 'jamshedvesuna/vim-markdown-preview'
"Plugin 'nlknguyen/papercolor-theme'
"Plugin 'Yggdroot/indentLine' "Potentiall breaks html syntax highlighting

" Might use again someday
"Plugin 'sjl/badwolf'
"Plugin 'dracula/vim'
"Plugin 'airblade/vim-gitgutter'
"Plugin 'chriskempson/base16-vim'
"Plugin 'vimwiki/vimwiki'
"Plugin 'marcopaganini/termschool-vim-theme'

call vundle#end()

" --- Plugin Settings


" -----------------------------------------------------------------------------
" Signify
" -----------------------------------------------------------------------------
set updatetime=100					" Increase update time
let g:signify_sign_change	="~"			" Change modified line symbol
highlight clear SignColumn				" set gutter bg collor to match editor
let g:signify_sign_show_text = 1
"highlight SignifySignAdd ctermfg=green cterm=NONE guifg=green gui=NONE
"highlight SignifySignDelete ctermfg=red cterm=NONE guifg=red gui=NONE
"highlight SignifySignChange ctermfg=yellow cterm=NONE guifg=yellow gui=NONE

" Settings to fix gutter highlighting 
highlight SignifySignAdd ctermfg=41 cterm=NONE 
highlight SignifySignDelete ctermfg=167 cterm=NONE 
highlight SignifySignChange ctermfg=30 cterm=NONE 
highlight SignColumn ctermbg=NONE cterm=NONE guibg=NONE gui=NONE
" -----------------------------------------------------------------------------
" Markdown
" -----------------------------------------------------------------------------
"let vim_markdown_preview_github=1
"let vim_markdown_preview_pandoc=1
"let vim_markdown_preview_toggle=2
"let vim_markdown_preview_use_xdg_open=1
"let vim_markdown_preview_hotkey='<C-m>'

" -----------------------------------------------------------------------------
" indent-guides
" -----------------------------------------------------------------------------
let g:indent_guides_enable_on_vim_startup=1
let g:indent_guides_guide_size=2
"let g:indent_guides_start_level=2
"
"hi IndentGuidesOdd ctermbg=red
"hi IndentGuidesOdd ctermbg=lightred

" -----------------------------------------------------------------------------
" Searchtasks
" -----------------------------------------------------------------------------
"  Set keywords that SearchTasks will look for
let g:searchtasks_list=["TODO", "FIXME", "NOTE", "FEATURE", "IDEA", "DONE"]

" -----------------------------------------------------------------------------
" NERDTree
" -----------------------------------------------------------------------------
let NERDTreeQuitOnOpen = 0

" -----------------------------------------------------------------------------
" Limelight
" -----------------------------------------------------------------------------
let g:limelight_conceal_ctermfg=244

" -----------------------------------------------------------------------------
" Indent-Lines
" -----------------------------------------------------------------------------
"let g:indentLine_enabled=1

" -----------------------------------------------------------------------------
" vim-devicons
" -----------------------------------------------------------------------------

" disable brackets around devicons 
" FIXME not working in iterm2 + tmux
let g:webdevicons_conceal_nerdtree_brackets = 0


"[eof]
