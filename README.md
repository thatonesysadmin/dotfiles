## DOTFILES

---
### About

My collection of dotfiles for my various Unix/Linux environments. Feel free to use them! Check [here](./docs/setup.md) for further details on the specific function of each file.

#### Requirements

| App | Requires | Link |
|:---|:---|---|
| **SHELLS** |||
|ZSH| Oh my ZSH| [Get it here](https://ohmyz.sh/)|
||Powerline|[Get it here]()|
|Bash| bash-completion||
| **EDITOR** |||
|Vim| Vundle ||
| **TERMINAL** |||
|TMUX |TMUX Vim Navigator|[Get it here](https://github.com/christoomey/vim-tmux-navigator)|

There are setup scripts for Linux, macOS, and Windows systems under the `os` directory. 

