#!/usr/bin/env bash

##############
# macOS system default settings config
#
# Inspired by:
#
# https://github.com/rgomezcasas/dotfiles/blob/master/mac/mac-os.sh
# https://github.com/lucaionescu/dotfiles/blob/master/macos/defaults.sh
# 
#
# -- More info on how these commands work and how to use them 
# https://support.apple.com/guide/terminal/edit-property-lists-apda49a1bb2-577e-4721-8f25-ffc0836f6997/mac
##############

### SYSTEM ###

# Hide all desktop icons by default (set to true to disable)
defaults write com.apple.finder CreateDesktop false

# // more code here

### SAFARI ###

# // more code here

### FINDER ###

# Show all files in Finder - includes hidden files (set to No to disable)
defaults write com.apple.finder AppleShowAllFiles Yes

# // more code here

# Restart Finder
killall Finder

# Configure system

## install homebrew
echo "Installing Homebrew Package Manager"
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
